<?php
/**
 * Created by PhpStorm.
 * User: Luis Ángel
 * Date: 13/05/2016
 * Time: 03:58 PM
 */

try {
    $m = new MongoClient();
    $db = $m->selectDB("ssma");
    $colName = date("dmy");
    $colName = 'col' . $colName;
    $collection = $db->selectCollection("testttt");
    $person = array("name" => "Joe", "age" => 20);
    $collection->insert($person);
} catch (MongoConnectionException $e){
    echo 'N' . $e;
}

$m->close();