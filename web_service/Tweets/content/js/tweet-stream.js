/*
 * @depends mootools-1.2.4-core.js
 * @depends mootools-1.2.4.4-more.js
 *
 * Created: 2010-08-20
 * Nathan Reed (c) 2010
 */

var _DATA_COOKIE = 'stream-data';
var _LANG_COOKIE = 'stream-lang';

var Ui = {
	init: function() {

		this.column_count = 0;
		this.max_cols = 5;
		this.streams = $H();
		this.default_columns = ['twitter', 'google'];

		// set the language, either from the #hashtag or the cookie
		// default to english if we have neither
		this.lang = location.hash.replace('#', '') || Cookie.read(_LANG_COOKIE) || 'en';

		// set the selected lang link as inactive, save the language to the cookie
		$('ll-'+this.lang).href = '#';
		$('ll-'+this.lang).setStyle('color', 'black');
		Cookie.write(_LANG_COOKIE, this.lang, {duration: 7});


		// read the cookie data to set up the page as it was last time
		var json_cookie = Cookie.read(_DATA_COOKIE);
		if($defined(json_cookie)) {
			this.fromJson(json_cookie);
		}

		// v1.1 UPDATE
		// since the v1.1 changes to the api, its much harder to get the trending
		// topics without having to do some sort of caching server side, so lets
		// just disable it for now and use some default terms
		//
		// start getting trending topics. If there are no columns on the page
		// when we are done, then add the top 3 TT.
		/*new TrendingTopics({
			onSuccess: function(data) {
				var trending_count = 3;
				Ui.load_trending(data[0].trends);

				if(this.column_count == 0) {
					for(var i=0; i < trending_count; i++) {
						this.add_column(data[0].trends[i].name, false);
					}
				}
			}.bind(Ui)
		});*/

		if(this.column_count == 0) {
			for(var i=0; i < this.default_columns.length; i++) {
				this.add_column(this.default_columns[i], false);
			}
		}

	},

	start_search: function(elem, stream_id) {
		var query = $(elem).value.trim();

		if(query != "") {
			this.streams[stream_id].start_feed(query);
		}
	},

	toggle_pause: function(elem, stream_id) {
		this.streams[stream_id].toggle_pause();
		var is_paused = this.streams[stream_id].is_paused;

		if(is_paused) {
			$(elem).innerHTML = 'resume';
		} else {
			$(elem).innerHTML = 'pause';
		}
	},

	clear_search: function(stream_id) {
		this.streams[stream_id].clear();
		$('t-search-' + (stream_id)).value = "";
		$('t-rate-' + (stream_id)).innerHTML = "0" + _RATE_SUFFIX;
	},

	add_column: function(search_query, will_save) {

		if(!$defined(will_save)) {
			// do we save this new column to the state cookie?
			will_save = true;
		}

		if(this.column_count >= this.max_cols) {
			return;
		}

		search_query = search_query || '';
		var column_id = 'C' + (Math.random() * 1000).round().toString();

		// add the column to the dom, and create the object to handle it.
		var new_elem = new JsTemplate('tmpl-tweet-col').render({'column_id': column_id, 'query':search_query}).inject('t-columns');

		this.streams[column_id] = new TweetStream('tweet-list-'+column_id, {
			'rate_elem': 't-rate-'+column_id,
			'lang':this.lang,
			'max_displayed':16,
			'popular_list': 'pop-tweets-'+column_id,
			'popular_alert': 'pop-tweets-alert-'+column_id
		}).start_feed(search_query);

		// we set this now instead of in the constructor so that we don't have to
		// worry about it getting triggered on the initial search
		this.streams[column_id].options.onChange = this.query_change.bind(this);
		this.column_count++;

		// resize all the columns to the correct width
		// edit: don't think this is needed anymore, just have them al at 400px?
		//var new_width = (90 / this.column_count).round() + '%';
		//$$('.tweet-col').setStyle('width', new_width);

		if(will_save) {
			// set the focus on the textbox we just added

			this.save_state();
		}

		if(search_query == "" && will_save) {
			// focus on the just added textbox, but only if it has been
			// added by the user
			$('t-search-'+column_id).focus();
		}
	},

	delete_column: function(stream_id) {
		this.streams[stream_id].stop();
		this.streams.erase(stream_id);

		$('tc-' + stream_id).dispose();

		this.column_count--;
		this.save_state();
	},

	query_change: function(stream, new_query) {
		// one of the searches has changed. save the change to the cookie
		this.save_state();
	},

	toggle_popular: function(stream_id) {
		var stream = this.streams[stream_id];
		stream.popular_visible = !stream.popular_visible;

		if(stream.popular_visible) {
			$('pop-tweets-alert-'+stream_id).innerHTML = 'click to hide popular tweets';
			$('pop-tweets-'+stream_id).setStyle('display', '');
		} else {
			$('pop-tweets-'+stream_id).setStyle('display', 'none');
			$('pop-tweets-alert-'+stream_id).innerHTML = stream.popular_alert_text;
		}
	},

	// serialize the current state of the streams to json so that they
	// can be saved to a cookie
	toJson: function() {
		var stream_meta_data = [];

		this.streams.each(function(item) {
			stream_meta_data.push({'query':item.search_query});
		});

		return JSON.encode(stream_meta_data);
	},

	// takes a json object and creates all the streams needed
	fromJson: function(json_data) {
		if($type(json_data) == 'string') {
			json_data = JSON.decode(json_data);
		}

		if($type(json_data) != 'array') {
			return this;
		}

		json_data.each(function(item) {
			// create each column
			this.add_column(item.query, false);
		}.bind(Ui));

		return this;
	},

	save_state: function() {
		// serialize the column state and save to cookie for next time
		Cookie.write(_DATA_COOKIE, this.toJson(), {duration: 7});
	},

	change_language: function(lang_code) {
		// set the hashtag, reload the page.
		// is this the best way to set the language? im not sure...
		window.location = '#' + lang_code;
		window.location.reload();
	},

	load_trending: function(trend_list) {
		var max_trending = 4;

		trend_list.each(function(item, index) {
			if(index >= max_trending) {
				return;
			}

			var new_link = new Element('a', {
				'href': 'javascript:void(0)',
				'text': item.name,
				'events': {
					'click': function() { Ui.add_column(item.name); }
				}
			});

			$('tt-list').grab(new_link)

			if(index != trend_list.length-1 && index != max_trending-1) {
				$('tt-list').appendText(' | ');
			}
		});

	}
}
