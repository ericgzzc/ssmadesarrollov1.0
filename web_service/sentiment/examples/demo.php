<?php
if (PHP_SAPI != 'cli') {
	echo "<pre>";
}

$strings = array(
	1 => '\"Creo firmemente que el liderazgo es tan fuerte como grande sea el sueno por cumplir\" Jaime Santibanez, Director Nacional de JA Mexico, estuvo presente en el desayuno empresarial del Tecnologico de Monterrey campus Morelos, hablando sobre \"Liderazgo Inspirador\"\nJA Morelos #transformamosvidas #juniorachievementmexico miembro de #jaworldwide #tecdemonterrey #liderazgoinspirador',
	2 => 'Amigos EXATEC #friends #family #fun #wednesday #night #happy #exatec #tecdemonterrey #master #school #crew #picoftheday #blue #pink #instalike',
	3 => 'Los vols del grupo local de Hidalgo estuvieron presentes en el Tec de Monterrey con motivo del \"Dia de la tierra\"\n#tecdemonterrey #hidalgo #greenpeace #diadelatierra #voluntarios #picoftheday #nofilter',
	4 => 'Modelo C40014. Precio mayoreo $99.00 mxn. Inicia tu negocio desde 1000 pesos!!!! Visita nuestro catalogo completo en facebook, tenemos mas de 2500 modelos de collares disponibles!!, buscanos como Efecto Mariposa Accesorios. #collares #mayoreo #outlet #accesorios #trendy #fashion #beautiful #puebla #cholula #mexico #ibero #udlap #tecdemonterrey #upaep #ventas #tunegocio #inversion #ventasporcatalogo #moda',
	5 => 'Somos 100% confiables , aqui les mostramos los comprobantes de envios del mes de Abril que hicimos por estafeta y Fedex  atendemos sus mensajes por DM o WA(222) 794 7134 ;  hacemos entregas personales en Puebla , DF y Morelos ; asi como envios GRATIS a todo Mexico via estafeta express te llega tu paquete  de 1 a 2 dias habiles y para tu  mayor seguridad y confianza se te proporciona un numero de guia y un codigo de rastreo para que via online cheques la ubicacion de tu paquete #udlap #tecdemonterrey #uvm #buap #iberopue #upaep #puebla #monterrey #df #Guadalajara #gdl #snl #veracruz #guerrero #mexico #querataro #cancun #tabasco #culiacan #durango',
);




require_once __DIR__ . '/../autoload.php';
$sentiment = new \PHPInsight\Sentiment();
foreach ($strings as $string) {

	// calculations:
	$scores = $sentiment->score($string);
	$class = $sentiment->categorise($string);

	// output:
	echo "String: $string\n";
	echo "Dominant: $class, scores: ";
	print_r($scores);
	echo "\n";
}
