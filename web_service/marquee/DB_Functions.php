<?php
/**
 * Created by PhpStorm.
 * User: Luis Ángel
 * Date: 21/04/2016
 * Time: 12:40 PM
 */

class DB_Function
{

    //<editor-fold desc="Funciones de la clase">
    private $db;

    // Constructor
    function __construct()
    {
        date_default_timezone_set('America/Monterrey');
        require_once 'DB_Connect.php';
        // conexion de base de datos
        $this->db = new DB_Connect();
        $this->db->connect();
        $GLOBALS["conexion"] = $this->db->connect();
    }

    // Destructor
    function __destruct()
    {
    }

    public function getData($word)
    {
        $apis = 't';
        $t = false;
        $i = false;
        $idate = '';
        $fdate='';
        // si encuentra la i en el post con los valores enviados de redes a buscar
        $pos = strpos($apis, 't');
        if ($pos !== false) {
            $array['api'] = 'twitter';
            $t = true;
        }

        $pos = strpos($apis, 'i');
        if ($pos !== false) {
            $array['api'] = 'instagram';
            $i = true;
        }
        $Query = array('$and' => array(array("text_clean" => array('$regex' => $word)), $array));
        // Si solo llegó una red social así se genera el query...si son más de dos redes, determinar la mejor manera de construir el array del OR
        if ($t && $i) {
            // $Query = array('$and' => array(array("text_clean" => array('$regex' => $word)), array('$or'=> array(array('api'=>'twitter'),array('api'=>'instagram')))));
            $Query = array("text_clean" => array('$regex' => $word));
        }

        $m = new MongoClient();

        $db = $m->selectDB("ssma");

        if ($idate != '') {
            $inidate = $idate;
            $inidate = new DateTime($inidate);
            $inidateStr = $inidate->format('dmy');
        } else {
            $inidate = new DateTime('2016-03-04');
            $inidateStr = $inidate->format('dmy');
        }

        if ($fdate != '') {
            $findate = new DateTime($fdate);
            $findate->add(new DateInterval('P1D'));

            $findate = $findate->format('dmy');
        } else {
            $findate = new DateTime();
            $findate->add(new DateInterval('P1D'));
            $findate = $findate->format('dmy');
        }

        $a = 0;
        $arr = array();

        while ($inidateStr != $findate) {
            $colName = 'col' . $inidateStr;
            $collection = $db->selectCollection($colName);

            $cursor = $collection->find($Query);

            foreach ($cursor as $col) {
                if (isset($col["id_post"])) {
                    $arr[$a]["id_post"] = $col["id_post"];
                } else {
                    $arr[$a]["id_post"] = "";
                }
                if (isset($col["likes"])) {
                    $arr[$a]["likes"] = $col["likes"];
                } else {
                    $arr[$a]["likes"] = "";
                }
                if (isset($col["cant_retweet"])) {
                    $arr[$a]["cant_retweet"] = $col["cant_retweet"];
                } else {
                    $arr[$a]["cant_retweet"] = "";
                }
                if (isset($col["cuentas_que_sigue"])) {
                    $arr[$a]["cuentas_que_sigue"] = $col["cuentas_que_sigue"];
                } else {
                    $arr[$a]["cuentas_que_sigue"] = "";
                }
                if (isset($col["cuentas_que_sigue"])) {
                    $arr[$a]["cuentas_que_sigue"] = $col["cuentas_que_sigue"];
                } else {
                    $arr[$a]["cuentas_que_sigue"] = "";
                }
                if (isset($col["cuentas_que_lo_siguen"])) {
                    $arr[$a]["cuentas_que_lo_siguen"] = $col["cuentas_que_lo_siguen"];
                } else {
                    $arr[$a]["cuentas_que_lo_siguen"] = "";
                }
                if (isset($col["friends_count"])) {
                    $arr[$a]["friends_count"] = $col["friends_count"];
                } else {
                    $arr[$a]["friends_count"] = "";
                }
                if (isset($col["Klout"])) {
                    $arr[$a]["Klout"] = $col["Klout"];
                } else {
                    $arr[$a]["Klout"] = "";
                }
                if (isset($col["text_clean"])) {
                    $arr[$a]["text_clean"] = $col["text_clean"];
                } else {
                    $arr[$a]["text_clean"] = "";
                }
                if (isset($col["created_at"]) && $col["created_at"] != '') {
                    //  $arr[$a]["created_at"] = date_format(date_create($col["created_at"]), "d/m/Y");
                } else {
                    $arr[$a]["created_at"] = "";
                }
                if (isset($col["id_usuario"])) {
                    $arr[$a]["id_usuario"] = $col["id_usuario"];
                } else {
                    $arr[$a]["id_usuario"] = "";
                }
                if (isset($col["nombre_usuario"])) {
                    $arr[$a]["nombre_usuario"] = $col["nombre_usuario"];
                } else {
                    $arr[$a]["nombre_usuario"] = "";
                }
                if (isset($col["screen_name"])) {
                    $arr[$a]["screen_name"] = $col["screen_name"];
                } else {
                    $arr[$a]["screen_name"] = "";
                }
                if (isset($col["foto_perfil"])) {
                    $arr[$a]["foto_perfil"] = $col["foto_perfil"];
                } else {
                    $arr[$a]["foto_perfil"] = "";
                }
                if (isset($col["sentiment"])) {
                    $arr[$a]["sentiment"] = $col["sentiment"];
                } else {
                    $arr[$a]["sentiment"] = "";
                }
                if (isset($col["url"])) {
                    $arr[$a]["url"] = $col["url"];
                } else {
                    $arr[$a]["url"] = "";
                }
                if (isset($col["type"])) {
                    $arr[$a]["type"] = $col["type"];
                } else {
                    $arr[$a]["type"] = "";
                }
                if (isset($col["location"])) {
                    $arr[$a]["location"] = $col["location"];
                } else {
                    $arr[$a]["location"] = "";
                }
                if (isset($col["api"])) {
                    $arr[$a]["api"] = $col["api"];
                } else {
                    $arr[$a]["api"] = "";
                }
                $arr[$a]["collection"] = $colName;

                $a++;
            }
            $inidate->add(new DateInterval('P1D'));
            $inidateStr = $inidate->format('dmy');
            //echo $inidateStr.' == '.$findate.'<br>';
        }

        $m->close();
        return $arr;
    }
}