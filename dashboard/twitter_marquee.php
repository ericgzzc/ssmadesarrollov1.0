<?php
/**
 * Created by PhpStorm.
 * User: Luis Ángel
 * Date: 21/04/2016
 * Time: 09:44 AM
 */

// https://markinns.com/archive/recreating-the-fade-scrolling-text-marquee-on-twitter.html
/*
 * #marquee {
    background:#eee;
    overflow:hidden;
    white-space: nowrap;
}
 */
echo '
<html>
<head>
<style>

div {
  display: inline-block;
  height: 300px;
  margin-left: 5px;
  width: 100%;
}

p {
    margin: 0;
    padding: 0;
}

#container {
    font-family: \'Gotham Narrow SSm A\', \'Gotham Narrow SSm B\';
    width: 450px;
    height: 80px;
    overflow: hidden;
    border-radius: 12px 12px 12px 12px;
    -moz-border-radius: 12px 12px 12px 12px;
    -webkit-border-radius: 12px 12px 12px 12px;
    border: 0px solid #292f33;
    background: #ffffff;
}
#container img {
    float: left;
    padding: 10px;
    height: 65px;
    width: 65px;
    border-radius:20px;
}

#container span {
    color: #000;
    font-weight: bold;
}

</style>
</head>
<body>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>';
echo '

<marquee scrollamount="7" >';
require_once "marquee/DB_Functions.php";
$db = new DB_Function();
$cols = $db->getData('itesm');
$tweetno = sizeof($cols);
for($i = 0; $i < 250; $i++) {
    if (isset($cols[$i]['foto_perfil']) && isset($cols[$i]['screen_name']) && isset($cols[$i]['text_clean']) && isset($cols[$i]["nombre_usuario"])
    && isset($cols[$i]["id_post"])){
        $enlace = "http://twitter.com/" . $cols[$i]['screen_name'] . "/status/".$cols[$i]['id_post'];
        echo '<a target="_blank" href="'. $enlace .'"><div id="container">
        <img src="' . $cols[$i]['foto_perfil'] . '" onerror="this.src=\'marquee/default.jpg\'" />
        <p style="color: #66757f;"><span>'. utf8_decode($cols[$i]["nombre_usuario"]) . '</span>  ' . $cols[$i]['screen_name'] . ' </p>
        <p style="color: #55acef;">' . utf8_decode($cols[$i]['text_clean']) . '</p>
    </div></a>';
    }
}
echo '
</marquee>
</body>
</html>
';

/*
 * <a class="twitter-timeline"  href="https://twitter.com/search?q=itesm" data-widget-id="723622696399998976">Tweets sobre itesm</a>
            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
 */