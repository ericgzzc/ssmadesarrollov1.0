<!DOCTYPE html>
<html>
<head>
    <title>jQCloud Example</title>
    <link rel="stylesheet" type="text/css" href="jqcloud.css" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script type="text/javascript" src="jqcloud-1.0.4.js"></script>
    <script src="../assets/sweetalert/sweetalert2.min.js" type="text/javascript"></script>
    <link href="../assets/sweetalert/sweetalert2.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="tagcloud.js"></script>
</head>
<body>
<!-- You should explicitly specify the dimensions of the container element -->
    <div class="container-fluid">
        <div id="example" style="height: 400px"></div>
    </div>
</body>

</html>