<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="/dashboard.css">

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>

    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
    <script src="https://www.amcharts.com/lib/3/pie.js"></script>
    <script src="https://www.amcharts.com/lib/3/serial.js"></script>
    <script src="https://www.amcharts.com/lib/3/amstock.js"></script>
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
    

</head>
<div class="row">
    <select required class="form-control" name="" id="wordsBD" style="width: 50%; display: inline;">
        <option value="" disabled="">Selecciona una opcion</option>
    </select>

    <button type="button" class="btn btn-primary" id="searchSentiment">Buscar</button>

</div>
<div class="row">

    <div class="col-md-6 col-sm-6 col-lg-6 col-xl-6" style="text-align: center" id="sentiment">

        <h4>Sentimiento</h4>
        <div id="loader" style="display: " class="lod2 lading">
            <div class="dot"></div>
            <div class="dot"></div>
            <div class="dot"></div>
            <div class="dot"></div>
            <div class="dot"></div>
            <div class="dot"></div>
            <div class="dot"></div>
            <div class="dot"></div>
            <div class="lading"></div>
        </div>
        <div style="display: none" id="sentimento">
            <h4 style="display:none;" id="h1">No existen resultados</h4>

        </div>
        <div id="donutSentiment" style="display: none "></div>


    </div>
    <div class="col-md-6 col-sm-6 col-lg-6 col-xl-6"  style="text-align: center">
        <h4>Indice Klout </h4>
        <div id="loader" style="display: " class="lod4">
            <div class="dot"></div>
            <div class="dot"></div>
            <div class="dot"></div>
            <div class="dot"></div>
            <div class="dot"></div>
            <div class="dot"></div>
            <div class="dot"></div>
            <div class="dot"></div>
            <div class="lading"></div>
        </div>
        <div id="kloutBar" style="display: none">
            <div id="countKlout"></div>
            <div class="container-fluid">
                <div class="row text-center" style="overflow:hidden;">
                    <div class="col-sm-3" style="float: none !important;display: inline-block;">
                        <label class="text-left">Angle:</label>
                        <input class="chart-input" data-property="angle" type="range" min="0" max="89"
                               value="30"
                               step="1"/>
                    </div>

                    <div class="col-sm-3" style="float: none !important;display: inline-block;">
                        <label class="text-left">Depth:</label>
                        <input class="chart-input" data-property="depth3D" type="range" min="1" max="120"
                               value="60"
                               step="1"/>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

