/**
 * Created by L03020776 on 29/04/2016.
 */
/*!
 * Create an array of word objects, each representing a word in the cloud

 var word_array = [
 {text: "Lorem", weight: 15},
 {text: "Ipsum", weight: 9, link: "http://jquery.com/"},
 {text: "Dolor", weight: 6, html: {title: "I can haz any html attribute"}},
 {text: "Sit", weight: 7},
 {text: "Amet", weight: 5}
 ];*/
/*
 $(function() {
 // When DOM is ready, select the container element and call the jQCloud method, passing the array of words as the first argument.
 $("#example").jQCloud(word_array);
 });*/

// idate:$("#startDate").val(),fdate:$("#endDate").val()
$.post( "../web_service/tag_cloud.php",{idate:'', fdate:''}, function(data) {
    //alert( "success" );
    console.log('tag_cloud: ' + data);
}, "json")
    .done(function(data) {
        console.log('tag_cloud done: ' + data);
        if(data == null){
            swal('No hay resultados');
            return;
        }

        $("#example").jQCloud(data);
    })
    .fail(function(jqXHR) {
        console.log('tag_cloud fail:' + jqXHR.responseText);
        swal("Algo sucedi�, no se alarme, por favor contacte a un administrador." );
    });