<!DOCTYPE html>
<link rel="stylesheet" type="text/css" href="/dashboard.css">

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/pie.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="https://www.amcharts.com/lib/3/amstock.js"></script>
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
<script src="dashboard.js"></script>

<div class="row">
    <div id="chartdiv" style="width: 100%; height: 355px; display: block;"></div>
</div>