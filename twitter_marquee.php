<?php
/**
 * Created by PhpStorm.
 * User: Luis Ángel
 * Date: 21/04/2016
 * Time: 09:44 AM
 */

// https://markinns.com/archive/recreating-the-fade-scrolling-text-marquee-on-twitter.html
/*
 * #marquee {
    background:#eee;
    overflow:hidden;
    white-space: nowrap;
}
 */
echo '
<html>
<head>
<style>

div {
  display: inline-block;
  height: 300px;
  margin-left: 5px;
  width: 100%;
}

#container {
    width: 450px;
    height: 80px;
    color: #0084b4;
    overflow: hidden;
    border-radius: 12px 12px 12px 12px;
    -moz-border-radius: 12px 12px 12px 12px;
    -webkit-border-radius: 12px 12px 12px 12px;
    border: 0px solid #4099ff;
    background: #c0deed;
}
#container img {
    float: left;
    padding: 10px;
    height: 50px;
    width: 50px;
}

</style>
</head>
<body>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script src="http://malsup.github.io/jquery.cycle2.js"></script>';
echo '

<marquee scrollamount="7">';
require_once "web_service/marquee/DB_Functions.php";
$db = new DB_Function();
$cols = $db->getData('itesm');
// echo json_encode($cols); //no se usa porque no esta encodeado en DB_Functions
$tweetno = sizeof($cols);
for($i = 0; $i < 150; $i++) {
    echo '<div id="container">
        <img src="' . $cols[$i]['foto_perfil'] . '" onerror="this.src=\'web_service/marquee/default.jpg\'" />
        <p>' . $cols[$i]['screen_name'] . ': ' . utf8_decode($cols[$i]['text_clean']) . '</p>
    </div>';
}
echo '
</marquee>
</body>
</html>
';

/*
 * <a class="twitter-timeline"  href="https://twitter.com/search?q=itesm" data-widget-id="723622696399998976">Tweets sobre itesm</a>
            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
 */